﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    class Person
    {
        public string FirstName1 { get; set; }
        public string LastName1 { get; set; }
        public int PhoneNumber1 { get; set; }

        public Person(string firstName, string lastName)
        {
            FirstName1 = firstName;
            LastName1 = lastName;
        }

        public Person(string firstName, string lastName, int phoneNumber)
        {
            FirstName1 = firstName;
            LastName1 = lastName;
            PhoneNumber1 = phoneNumber;
        }

        public bool NameContains(string search)
        {

            if (FullName().Contains(search, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        public string FullName()
        {
            return FirstName1 + " " + LastName1;
        }
    }
}
