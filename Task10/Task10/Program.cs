﻿using System;
using System.Collections.Generic;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            // Setup
            List<Person> persons = new List<Person>();
            bool searchMatch = false;

            // Sample persons:
            persons.Add(new Person("John", "Doe"));
            persons.Add(new Person("Pete", "Johnson"));
            persons.Add(new Person("Frank", "Ogre"));
            persons.Add(new Person("Obama", "Underhill", 95959595));
            persons.Add(new Person("Frank", "Castle"));

            // Get the name the user wishes to search for
            Console.WriteLine("Please enter the name you wish to search for: ");
            string search = Console.ReadLine().ToLower();


            // Preparing output
            Console.WriteLine("----Results----");


            // Iterate through the persons and output all names that partly match
            foreach (Person p in persons)
            {

                if(p.NameContains(search))
                {
                    Console.WriteLine(p.FullName());
                    searchMatch = true;
                }
            }
            // If no match is found, inform the user
            if (!searchMatch)
            {
                Console.WriteLine("No match found");
            }
        }
    }
}
