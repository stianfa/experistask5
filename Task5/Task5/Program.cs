﻿using System;
using System.Collections.Generic;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Setup
            List<(string, string)> names = new List<(string, string)>();
            bool searchMatch = false;

            // Sample names:
            names.Add(("John", "Doe"));
            names.Add(("Pete", "Johnson"));
            names.Add(("Frank", "Ogre"));
            names.Add(("Obama", "Stillhill"));
            names.Add(("Frank", "Castle"));

            // Get the name the user wishes to search for
            Console.WriteLine("Please enter the name you wish to search for: ");
            string search = Console.ReadLine().ToLower();

            // Preparing output
            Console.WriteLine("----Results----");

            // Iterate through names and output all names that partly match
            foreach ((string,string) name in names)
            {

                string fullName = (name.Item1 + " " + name.Item2);
                
                if(search.Equals(fullName.ToLower()) || name.Item1.ToLower().Contains(search) || name.Item2.ToLower().Contains(search))
                {
                    
                    Console.WriteLine(fullName);

                    searchMatch = true;
                }
            }
            // If no match is found, inform the user
            if(!searchMatch)
            {
                Console.WriteLine("No match found");
            }
        }
    }
}
